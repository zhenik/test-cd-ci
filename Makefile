
gr-help:
	docker run --rm -t -i gitlab/gitlab-runner --help

gr-volume:
	docker volume create gitlab-runner-config

gr-start:
	docker run -d --name gitlab-runner --restart always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v gitlab-runner-config:/etc/gitlab-runner \
        gitlab/gitlab-runner:latest

gr-register:
	docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
